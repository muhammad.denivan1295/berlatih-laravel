<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    //
    public function create()
    {
        return view('pertanyaanCreate.create');
    }
    
    public function store(Request $request) 
    {
        $request->validate([
            'judul'=>'required|unique:pertanyaan',
            'isi'=>'required'
        ]);
        $query = DB::table('pertanyaan')->insert([
            'judul'=>$request['judul'],  // Name of the table column, followed by the form's ID
            'isi'=>$request['isi']        
            ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil disimpan!');
    }

    public function index() 
    {
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('pertanyaanCreate.index', compact('pertanyaan'));
    }

    public function show($id)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaanCreate.show', compact('pertanyaan'));
    }

    public function edit($id)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaanCreate.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul'=>'required|unique:pertanyaan',
            'isi'=>'required'
        ]);
        $query = DB::table('pertanyaan')
               ->where('id', $id)
               ->update([
            'judul'=>$request['judul'],  // Name of the table column, followed by the form's ID
            'isi'=>$request['isi']        
            ]);
        return redirect('/pertanyaan')->with('success', 'Suntingan berhasil disimpan!');
    }

    public function destroy($id)
    {
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil dihapus.');
    }
}
