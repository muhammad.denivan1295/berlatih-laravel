@extends('AdminLTE.master')

@section('content')
   <div class="mt-3 ml-3">
       <h4> {{$pertanyaan->judul}} <h4>
       <p> {{$pertanyaan->isi}} </p>
       <a href="/pertanyaan" class="btn btn-danger mt-2">Kembali ke Layar Utama</a>
    </div>   
@endsection