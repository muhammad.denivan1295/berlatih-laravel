@extends('AdminLTE.master')

@section('content')
<div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Sunting Pertanyaan {{$pertanyaan->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
        @csrf
        @method('PUT')
      <div class="box-body">
        <div class="form-group">
          <label for="title">Judul</label>
          <input type="text" class="form-control" id="title" name="judul" value=" {{(old('judul', $pertanyaan->judul))}} " placeholder="Masukkan judul pertanyaan disini...">
          @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="body">Isi Pertanyaan</label>
          <input type="text" class="form-control" id="body" name="isi" value=" {{(old('isi', $pertanyaan->isi))}} " placeholder="Detail pertanyaan">
          @error('isi')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
      <!-- /.card-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Simpan Suntingan</button>
      </div>
    </form>
  </div>
    
@endsection