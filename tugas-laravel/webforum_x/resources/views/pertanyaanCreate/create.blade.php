@extends('AdminLTE.master')

@section('content')
<div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Buat Pertanyaan Baru</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan/create" method="POST">
        @csrf
      <div class="box-body">
        <div class="form-group">
          <label for="title">Judul</label>
          <input type="text" class="form-control" id="title" name="judul" value="{{(old('judul', ))}}" placeholder="Masukkan judul pertanyaan disini...">
          @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="body">Isi Pertanyaan</label>
          <input type="text" class="form-control" id="body" name="isi" value="{{(old('isi', ))}}" placeholder="Detail pertanyaan">
          @error('isi')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
      <!-- /.card-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Buat Pertanyaan</button>
      </div>
    </form>
  </div>
@endsection