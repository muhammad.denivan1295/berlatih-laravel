@extends('AdminLTE.master')

@section('content')
    <div class="mt-3 ml-3">
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Judul dan Isi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
              @endif
              <a href="/pertanyaan/create" class="btn btn-primary mb-2">Buat Pertanyaan Baru!</a>
              <table class="table table-bordered">
                <tbody><tr>
                  <th style="width: 10px">#</th>
                  <th>Judul</th>
                  <th>Isi Pertanyaan</th>
                  <th style="width: 40px">Action</th>
                </tr>
                @forelse($pertanyaan as $key => $questions)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $questions->judul }}</td>
                        <td>{{ $questions->isi }}</td>
                        <td style="display: flex;">
                            <a href="/pertanyaan/{{ $questions->id }}" class="btn btn-info btn-sm">Tunjukkan di Database</a>
                            <a href="/pertanyaan/{{ $questions->id }}/edit" class="btn btn-default btn-sm">Sunting di Database</a>
                            <form style="display: flex;" action="/pertanyaan/{{$questions->id}}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="Hapus dari Database" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4" align="center">Belum ada pertanyaan. Mari buat pertanyaan!</td>
                    </tr>
                @endforelse
              </tbody></table>
            </div>
            <!-- /.box-body -->
            <!-- <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">«</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">»</a></li>
              </ul>
            </div> -->
          </div>
    </div>
@endsection