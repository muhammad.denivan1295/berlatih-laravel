<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Homepage (Landing page) index
Route::get('/', 'HomeController@index');

// Sign Up page index
Route::get('/register', 'AuthController@landingregister');

// For POST data pushing, we use this route function
Route::post('/register', 'AuthController@welcomemessage');

// Displaying results and welcoming message
Route::post('/welcome', 'AuthController@welcomemessage');