<!DOCTYPE html>
<html>
  <head>
    <title>Welcome!</title> <!-- Website Title -->
    <meta charset="UTF-8">
    </head>
    <body>
      <!-- Headers -->
      <div>
        <!-- Throws first and last name into a full name string -->
        <h1>Selamat Datang, {{$firstname}} {{$lastname}}!</h1>
        <h2>Terima kasih telah bergabung di SanberBook! </h2>
      </div>
    </body>
</html>
