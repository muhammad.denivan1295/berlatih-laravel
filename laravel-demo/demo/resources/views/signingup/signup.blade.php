<!DOCTYPE html>
<html>
  <head>
    <title>SanberBook Sign Up Form</title>
    <meta charset="UTF-8">
    </head>
    <body>
      <!-- Headers -->
      <div>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h2>
      </div>

      <!-- Content -->
      <div>
        <form action="/welcome" method="POST">
          <!-- CSRF Protection -->
          @csrf
          <!-- First & Last Name Entry Form -->
          <div>
            <label for="TI_FirstName">First Name</label><br><br>
              <input type="text" placeholder="Enter your first name here..." value="" id="TI_FirstName" name="firstname">
            <br><br>
            <label for="TI_LastName">Last Name</label><br><br>
              <input type="text" placeholder="Enter your last name here..." value="" id="TI_LastName" name="lastname">
            <br><br>
          </div>
          <!-- Other Details' Form -->
          <div>
            <label>Gender:</label>
              <br><br>
                <input type="radio" name="RB_JenisKelamin" value="0"> Male <br>
                <input type="radio" name="RB_JenisKelamin" value="1"> Female <br>
                <input type="radio" name="RB_JenisKelamin" value="2"> Non-binary <br>
                <input type="radio" name="RB_JenisKelamin" value="3"> Rather not say 
              <br><br>
            <label>Nationality</label>
              <br><br>
                <select>
                  <option value="indo">Indonesian</option>
                  <option value="malay">Malaysian</option>
                  <option value="sg">Singaporean</option>
                  <option value="aus">Australian</option>
                  <option value="thai">Thai</option>
                  <option value="ph">Philippino</option>
                  <option value="oth">Others</option>
                </select>
              <br><br>
            <label>Language Spoken:</label>
              <br><br>
                <input type="checkbox" name="CB_LangSp" value="0"> Indonesian <br>
                <input type="checkbox" name="CB_LangSp" value="1"> English <br>
                <input type="checkbox" name="CB_LangSp" value="2"> Others <br>
              <br><br>
            <label for="TA_Bio">Bio</label>
              <br><br>
                <textarea cols="70" rows="10" placeholder="Write something about yourself!" id="TA_Pesan"></textarea>
              <br><br>
            <input type="submit" value="Sign Up!">
          </div>
        </form>
      </div>
      </body>
</html>
