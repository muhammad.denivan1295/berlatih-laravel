<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    // Code start

    public function index()
    {
        return view('index.home');
    }
}
