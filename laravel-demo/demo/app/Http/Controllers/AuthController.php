<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    // Code start

    // Returning to view Sign Up form page

    public function landingregister() 
    {
        return view('signingup.signup');
    }

    // Login request and user's full name thrower

    public function welcomemessage(Request $request)
    {
        $firstname = $request->firstname;
        $lastname = $request->lastname;

        return view('welcomemessage.welcome', compact('firstname', 'lastname'));
    }


    // Returning to view Welcome message page after signing up



   
}
